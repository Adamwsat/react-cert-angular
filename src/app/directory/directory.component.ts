import {FlatTreeControl} from '@angular/cdk/tree';
import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { Employee } from '../employee';
import {MatTreeFlatDataSource, MatTreeFlattener} from '@angular/material/tree';

/** Flat node with expandable and level information */
interface ExampleFlatNode {
  expandable: boolean;
  name: string;
  id: number;
  level: number;
}

/**
 * @title Tree with flat nodes
 */
@Component({
  selector: 'app-directory',
  templateUrl: './directory.component.html',
  styleUrls: ['./directory.component.css']
})
export class DirectoryComponent implements OnInit {
  employees: Employee[] = [];

  private _transformer = (node: Employee, level: number) => {
    return {
      expandable: !!node.reports && node.reports.length > 0,
      name: node.name,
      id: node.id,
      level: level,
    };
  }

  treeControl = new FlatTreeControl<ExampleFlatNode>(
      node => node.level, node => node.expandable);

  treeFlattener = new MatTreeFlattener(
      this._transformer, node => node.level, node => node.expandable, node => node.reports);

  dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

  constructor(private dataService: DataService) { }

  ngOnInit(): void {
    this.dataService.sendGetDirectoryRequest().subscribe((data: any)=>{
      this.dataSource.data = data;
    })
  }

  hasChild = (_: number, node: ExampleFlatNode) => node.expandable;
}
