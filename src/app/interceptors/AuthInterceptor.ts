import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      if(sessionStorage.getItem('jwt') != null) {
        request = request.clone({
          setHeaders: {
            Authorization: `Bearer ${sessionStorage.getItem('jwt')}`
          }
        });
      }
    return next.handle(request);
    }
}
