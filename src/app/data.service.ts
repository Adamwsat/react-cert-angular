import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Employee } from './employee';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private REST_API_SERVER = "http://localhost:8080/";
  private DIRECTORY_API = this.REST_API_SERVER + "documents/1";
  private EMPLOYEE_API = this.REST_API_SERVER + "employees/";
  private LOGIN_API = this.REST_API_SERVER + "auth/login";
  private httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      })
    };

  constructor(private httpClient: HttpClient) { }

  public sendGetLoginRequest(){
    //TODO: Credentials should obviously never go in a file, a login page would be preferred here
    return this.httpClient.post<any>(this.LOGIN_API, { username: 'user', password: 'pass' }).subscribe(data => {
            sessionStorage.setItem('currentUser', 'user');
            sessionStorage.setItem('jwt', data.token);
        })
  }

  public ensureLoggedIn(){
    if(sessionStorage.getItem('jwt') === null) {
      this.sendGetLoginRequest();
    }
  }

  public logOut(){
    sessionStorage.removeItem("jwt");
  }

  public sendGetDirectoryRequest(){
    this.ensureLoggedIn();
    return this.httpClient.get<Employee[]>(this.DIRECTORY_API);
  }

  public sendGetEmployeeListRequest(){
    this.ensureLoggedIn();
    return this.httpClient.get<Employee[]>(this.EMPLOYEE_API);
  }

  public sendGetEmployeeRequest(id : number){
    this.ensureLoggedIn();
    return this.httpClient.get<Employee>(this.EMPLOYEE_API + id);
  }

  public sendPutEmployeeRequest(id : number, employee : Employee){
    this.ensureLoggedIn();
    return this.httpClient.put<Employee>(this.EMPLOYEE_API + id, JSON.stringify(employee), this.httpOptions);
  }

  public sendPostEmployeeRequest(employee : Employee){
   this.ensureLoggedIn();
   return this.httpClient.post<Employee>(this.EMPLOYEE_API, JSON.stringify(employee), this.httpOptions);
  }

  public sendDeleteEmployeeRequest(id : number){
    this.ensureLoggedIn();
    return this.httpClient.delete<Employee>(this.EMPLOYEE_API + id);
  }
}
